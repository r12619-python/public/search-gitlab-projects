import sqlite3


class SqliteDB(object):
    """ подключение к базе SQLite, реализация базовых возможностей
        по выполнению запросов и предоставлению результатов """

    def __init__(self, db):
        """ подключение к SQLite базе """
        self.connect = sqlite3.connect(db)
        self.cursor = self.connect.cursor()

    def __del__(self):
        """ уничтожение объектов класса при удалении экземпляра """

        try:
            self.cursor.close()
        except Exception as e:
            print('cursor.close Exception:', str(e))

        try:
            self.connect.close()
        except Exception as e:
            print('connect.close Exception:', str(e))

    @staticmethod
    def cursor_to_dict(cursor):
        """ Return all rows from a cursor as a dict """
        columns = [col[0] for col in cursor.description]
        res = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
        return res

    @staticmethod
    def cursor_to_list(cursor):
        """ Return all rows from a cursor as a list """
        res = [
            row[0]
            for row in cursor.fetchall()
        ]
        return res

    def execute_sql(self, sql, *args, **kwargs):
        """ выполняет произвольный запрос к базе """
        try:
            self.cursor.execute(sql, *args, **kwargs)
            self.connect.commit()
        except Exception as e:
            print('execute_sql Exception:', str(e))
        return self

    def get_data_from_sql(self, sql, *args, **kwargs):
        """ выполняет SQL запрос и записывает результат в self.data """
        self.execute_sql(sql, *args, **kwargs)
        return self

    def to_dict(self):
        """ Возвращает результат выполнения запроса в виде словаря """
        return self.cursor_to_dict(self.cursor)

    def to_list(self):
        """ Возвращает результат выполнения запроса в виде списка """
        return self.cursor_to_list(self.cursor)


class TestDB(SqliteDB):
    """ реализует методы проекта TestDB, вызов процедур и функций SQL """

    def __init__(self, db):
        super().__init__(db)
        self.create_table_projects()
        self.create_table_substrings()

    def create_table_projects(self):
        """ Создаёт таблицу projects если такая не существует """
        sql = """
            CREATE TABLE IF NOT EXISTS projects (
                id INTEGER NOT NULL PRIMARY KEY,
                name TEXT,
                description TEXT,
                last_activity_at DATETIME,
                created_at TIMESTAMP
            );
        """
        self.execute_sql(sql)
        return self

    def create_table_substrings(self):
        """ Создаёт таблицу substrings если такая не существует """
        sql = """
            CREATE TABLE IF NOT EXISTS substrings (
                substring TEXT NOT NULL PRIMARY KEY
            );
        """
        self.execute_sql(sql)
        return self

    def get_projects(self, substring):
        """ Возвращает список проектов по ключевому слову """
        sql = "SELECT * FROM projects WHERE name LIKE :substring OR description LIKE :substring"
        return self.get_data_from_sql(sql, {"substring": '%'+substring+'%'}).to_dict()

    def get_substrings(self):
        """ Возвращает список использованных ключевых слов """
        sql = "SELECT * FROM substrings"
        return self.get_data_from_sql(sql).to_list()

    def add_projects(self, data):
        """ Добавляет список проектов в базу """
        sql = """INSERT INTO projects
                 SELECT :id, :name, :description, :last_activity_at, :created_at
                 WHERE :id NOT IN (SELECT id FROM projects)"""
        for project in data:
            if type(project) == dict:
                self.cursor.execute(sql, project)
                self.connect.commit()
        return self

    def add_substring(self, substring):
        """ Добавляет строку поиска в базу """
        sql = """INSERT INTO substrings
                 SELECT :substring
                 WHERE :substring NOT IN (SELECT substring FROM substrings)"""
        self.cursor.execute(sql, {'substring': substring})
        self.connect.commit()
        return self


if __name__ == "__main__":
    pass
