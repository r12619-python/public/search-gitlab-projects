import requests
from datetime import datetime


def get_projects(ps):
    """ Возвращает необходимые поля из полученного набора """
    projects = []
    for p in ps:
        project = {'id': p.setdefault('id'),
                   'name': p.setdefault('name'),
                   'description': p.setdefault('description'),
                   'last_activity_at': p.setdefault('last_activity_at'),
                   'created_at': datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+'Z'}
        if project['id']:
            projects.append(project)
    return projects


def projects_search(substring):
    """ Возвращает список проектов по ключевому слову """
    url = 'https://gitlab.com/api/v4/projects/'
    projects = []
    response = requests.get(url, params={'search': substring})
    if response.ok:
        projects = get_projects(response.json())
    return projects


if __name__ == '__main__':
    pass
